# OpenML dataset: mildew_8

https://www.openml.org/d/45161

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Mildew Bayesian Network. Sample 8.**

bnlearn Bayesian Network Repository reference: [URL](https://www.bnlearn.com/bnrepository/discrete-medium.html#mildew)

- Number of nodes: 35

- Number of arcs: 46

- Number of parameters: 540150

- Average Markov blanket size: 4.57

- Average degree: 2.63

- Maximum in-degree: 3

**Authors**: A. L. Jensen and F. V. Jensen.

**Please cite**: ([URL](https://www.semanticscholar.org/paper/MIDAS%3A-An-Influence-Diagram-for-Management-of-in-Jensen-Jensen/a08ce2d88c66bb5bf7968bdadee87c2f91caeb2c)): A. L. Jensen and F. V. Jensen. MIDAS - An Influence Diagram for Management of Mildew in Winter Wheat. Proceedings of the Twelfth Conference on Uncertainty in Artificial Intelligence (UAI1996), pages 349-356.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45161) of an [OpenML dataset](https://www.openml.org/d/45161). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45161/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45161/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45161/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

